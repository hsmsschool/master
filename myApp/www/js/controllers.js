angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('detailSongCtrl', function($scope, $stateParams, $cordovaSQLite) {
      $scope.init = function(){
              $scope.songObject = [];
              $scope.search = null;

              $scope.codeSong  = $stateParams.playlistId;
              //$scope.load(null);
              $scope.loadSong($stateParams.playlistId);
      };

      $scope.loadSong = function(code){
            var query = "SELECT * FROM song WHERE code = ?";
            $cordovaSQLite.execute(db, query , [code]).then(function(res) {
                      console.log("Total Records in l: " + res.rows.length);
                      if (res.rows.length > 0) {   
                          $scope.songObject = res.rows.item(0);
                      }else{
                           $scope.songObject = null;
                      }
                  },
                  function(error) {
                      //$scope.statusMessage = "Error on loading: " + error.message;
            });

      };
})


.controller('baihatuuthichCtrl', function($scope, $cordovaSQLite) {
      $scope.init = function(){
          $scope.listDanhSach = [];
          $scope.load();
      };

      $scope.load = function(){
        //return null;
            var query = "SELECT * FROM song WHERE liked = ? ORDER BY name DESC";
            $cordovaSQLite.execute(db, query , ['true']).then(function(res) {
                      console.log("Total Records in l: " + res.rows.length);
                      if (res.rows.length > 0) {   
                          //$scope.listDanhSach = res.rows; 
                          for (var i = 0; i <= res.rows.length; i++) {
                              $scope.listDanhSach.push(res.rows.item(i));
                          }
                      }else{
                          $scope.listDanhSach = [];
                      }
                  },
                  function(error) {
                      $scope.statusMessage = "Error on loading: " + error.message;
                    });
            };
          $scope.likeSong = function(item){
              var queryUpdate = 'UPDATE song SET liked = value1 WHERE code = ?';
              $scope.isLiked = false;
              $cordovaSQLite.execute(db, "SELECT liked FROM song WHERE code = ? ORDER BY name DESC" , [item.code]).then(function(res) {                    
                        if (res.rows.length > 0) {                                                   
                            $scope.isLiked = angular.copy(res.rows.item(0).liked);
                            $scope.updateLikeValueofSong(!(String($scope.isLiked) == "true"), item.code);                        
                            //item.liked = $scope.isLiked == 'true' ? 'false' : 'true'     
                            $scope.listDanhSach = [];                   
                            $scope.load();
                        }else{
                            console.log('Error happen lieksong! Ops..');
                        }
                    },
                    function(error) {
                        $scope.statusMessage = "Error on loading: " + error.message;
              });  
          }


          $scope.updateLikeValueofSong = function(value, songCode){
              $cordovaSQLite.execute(db, "UPDATE song SET liked = ? WHERE code = ?" , [value, songCode]).then(function(res) {                    
                        if (res.rowsAffected == 1) {                                                   
                            console.log('Update successful!');
                            return true;
                        }else{
                            console.log('Error happen! Ops..');
                            return false;
                        }
                    },
                    function(error) {
                        console.log('Error happen! Ops..');
                        return false;
              });  
          }            
})


/* Customize Controllers*/
.controller('traBaiHatCtrl', function($scope, $ionicScrollDelegate, filterFilter, $location, $anchorScroll, $cordovaSQLite) {

      $scope.init = function(){
          $scope.listDanhSach = [];
          $scope.search = null;
          //$scope.load(null);

      };


      //$scope.listDanhSach = [{code: '59705', name: 'Nguoi ve tu long dat'}, {code: '51105', name: 'Nguoi ve tu long dat'}, {code: '51105', name: 'Nguoi ve tu long dat'}];

      $scope.likeSong = function(item){
          var queryUpdate = 'UPDATE song SET liked = value1 WHERE code = ?';
          $scope.isLiked = false;
          $cordovaSQLite.execute(db, "SELECT liked FROM song WHERE code = ? ORDER BY name DESC" , [item.code]).then(function(res) {                    
                    if (res.rows.length > 0) {                                                   
                        $scope.isLiked = angular.copy(res.rows.item(0).liked);
                        $scope.updateLikeValueofSong(!(String($scope.isLiked) == "true"), item.code);                        
                        item.liked = $scope.isLiked == 'true' ? 'false' : 'true'                        
                    }else{
                        console.log('Error happen lieksong! Ops..');
                    }
                },
                function(error) {
                    $scope.statusMessage = "Error on loading: " + error.message;
          });  
      }


      $scope.updateLikeValueofSong = function(value, songCode){
          $cordovaSQLite.execute(db, "UPDATE song SET liked = ? WHERE code = ?" , [value, songCode]).then(function(res) {                    
                    if (res.rowsAffected == 1) {                                                   
                        console.log('Update successful!');
                        return true;
                    }else{
                        console.log('Error happen! Ops..');
                        return false;
                    }
                },
                function(error) {
                    console.log('Error happen! Ops..');
                    return false;
          });  
      }

      $scope.load = function(searchKey){
        //return null;
        searchKey = "%"+ searchKey+ "%";
        if (searchKey == null) {
            $cordovaSQLite.execute(db, 'SELECT * FROM song ORDER BY code DESC').then(function(res) {
                      console.log("Total Records in l: " + res.rows.length);
                      if (res.rows.length > 0) {                                                  
                          $scope.listDanhSach = res.rows;                          
                      }else{
                          $scope.listDanhSach = [];
                      }
                  },
                  function(error) {
                      $scope.statusMessage = "Error on loading: " + error.message;
            });            
        }
        else {

            if ($scope.listDanhSach != null && $scope.listDanhSach.length > 0) {
                $scope.listDanhSach.splice(0,$scope.listDanhSach.length);
            };
            var query = "SELECT * FROM song WHERE name LIKE ? ORDER BY name DESC";
            $cordovaSQLite.execute(db, query , [searchKey]).then(function(res) {
                      console.log("Total Records in l: " + res.rows.length);
                      if (res.rows.length > 0) {   
                          //$scope.listDanhSach = res.rows; 
                          for (var i = 0; i <= res.rows.length; i++) {
                              $scope.listDanhSach.push(res.rows.item(i));
                          }
                      }else{
                          $scope.listDanhSach = [];
                      }
                  },
                  function(error) {
                      $scope.statusMessage = "Error on loading: " + error.message;
            });
        }
            
      };      

      $scope.select = function(lastname) {
        var query = "SELECT code, name FROM song WHERE code = ?";
        $cordovaSQLite.execute(db, query, [lastname]).then(function(res) {
          angular.forEach(res.rows, function(item){
                console.log("SELECTED -> " + item.firstname + " " + item.lastname);
          });
            /*if(res.rows.length > 0) {
                console.log("SELECTED -> " + res.rows.item(0).firstname + " " + res.rows.item(0).lastname);
            } else {
                console.log("No results found");
            }*/
        }, function (err) {
            console.error(err);
        });
    }

      

      /*$cordovaSQLite.execute(db, 'SELECT * FROM song ORDER BY code DESC').then(function(res) {
                    console.log("Total Records: " + res.rows.length);
                    if (res.rows.length > 0) {
                        $scope.listDanhSach = res.rows;
                    }else{
                        $scope.listDanhSach = null;
                    }
                },
                function(error) {
                    $scope.statusMessage = "Error on loading: " + error.message;
          });
        $scope.listDanhSach = 
        [{"id":1,"first_name":"Patrick","last_name":"Adams","country":"Cyprus","ip_address":"153.88.89.148","email":"progers@yata.net"},
        {"id":2,"first_name":"Janet","last_name":"Burns","country":"Croatia","ip_address":"209.73.121.212","email":"jgordon@skivee.biz"},
        {"id":3,"first_name":"Kathy","last_name":"Chancey","country":"Armenia","ip_address":"164.214.217.162","email":"khamilton@rhynyx.biz"},
        {"id":4,"first_name":"Stephanie","last_name":"Dennis","country":"Mauritius","ip_address":"8.199.242.67","email":"sjohnson@jabbertype.mil"},
        {"id":5,"first_name":"Jerry","last_name":"Edwards","country":"Thailand","ip_address":"230.207.100.163","email":"jpalmer@avamm.org"},
        {"id":6,"first_name":"Lillian","last_name":"Franklin","country":"Germany","ip_address":"150.190.116.1","email":"lfranklin@eare.mil"},
        {"id":7,"first_name":"Melissa","last_name":"Gordon","country":"Serbia","ip_address":"162.156.29.99","email":"mgordon@flashset.org"},
        {"id":8,"first_name":"Sarah","last_name":"Harris","country":"Grenada","ip_address":"13.177.156.223","email":"sburns@eimbee.info"},
        {"id":9,"first_name":"Willie","last_name":"Ingles","country":"Croatia","ip_address":"115.133.81.82","email":"wburton@dynazzy.info"},
        {"id":10,"first_name":"Tina","last_name":"Johnson","country":"United States Virgin Islands","ip_address":"113.49.63.18","email":"tsimmons@devpulse.mil"},
        {"id":11,"first_name":"Kenneth","last_name":"Kent","country":"Mexico","ip_address":"92.89.76.196","email":"klarson@browseblab.info"},
        {"id":12,"first_name":"Philip","last_name":"Lyles","country":"Cuba","ip_address":"223.180.48.70","email":"pwelch@skippad.edu"},
        {"id":13,"first_name":"Nicholas","last_name":"Marker","country":"British Indian Ocean Territory","ip_address":"200.150.119.13","email":"nparker@twitternation.net"},
        {"id":14,"first_name":"Nicole","last_name":"Nebb","country":"Moldova","ip_address":"47.66.237.205","email":"nwebb@midel.biz"},
        {"id":15,"first_name":"Clarence","last_name":"Olsen","country":"China","ip_address":"134.84.246.67","email":"cschmidt@dazzlesphere.net"},
        {"id":16,"first_name":"Jessica","last_name":"Peterson","country":"Sao Tome and Principe","ip_address":"211.30.32.109","email":"jmurray@jumpxs.net"},
        {"id":17,"first_name":"Willie","last_name":"Quite","country":"US Minor Outlying Islands","ip_address":"158.40.109.208","email":"wschmidt@babbleset.edu"},
        {"id":18,"first_name":"Margaret","last_name":"Robertson","country":"Bhutan","ip_address":"252.123.77.101","email":"mevans@voolia.info"},
        {"id":19,"first_name":"Arthur","last_name":"Simmons","country":"Faroe Islands","ip_address":"116.5.126.29","email":"amorales@brainlounge.biz"},
        {"id":20,"first_name":"Charles","last_name":"55063","country":"Italy","ip_address":"10.43.255.4","email":"cperez@avaveo.net"},
        {"id":21,"first_name":"Jeffrey","last_name":"Turner","country":"Liechtenstein","ip_address":"55.140.114.8","email":"jwebb@mynte.net"},
        {"id":22,"first_name":"Andrea","last_name":"Upton","country":"Nauru","ip_address":"22.243.12.86","email":"asimpson@browsetype.mil"},
        {"id":23,"first_name":"Steve","last_name":"15063","country":"Morocco","ip_address":"21.166.38.112","email":"sreynolds@topiclounge.biz"},
        {"id":24,"first_name":"Gerald","last_name":"Veyes","country":"Isle of Man","ip_address":"235.115.15.46","email":"greyes@voolith.biz"},
        {"id":25,"first_name":"Judy","last_name":"Washington","country":"Sweden","ip_address":"39.120.240.182","email":"jwashington@oyondu.net"},
        {"id":26,"first_name":"Brandon","last_name":"Xi","country":"Vietnam","ip_address":"18.176.165.38","email":"bpatterson@skyba.org"},
        {"id":27,"first_name":"Brandon","last_name":"Yore","country":"Vietnam","ip_address":"18.176.165.38","email":"batterson@skyba.org"},
        {"id":28,"first_name":"Brandon","last_name":"Zeff","country":"Vietnam","ip_address":"18.176.165.38","email":"bpatterson@skyba.org"}];
        */
    
    $scope.scrollTop = function() {

        $scope.load($scope.search);
        $ionicScrollDelegate.scrollTop();
    };
    $scope.clearSearch = function($cordovaSQLite) {
        //scope.delteAll();
        //$scope.listInsert();
        //$scope.insert('59962', 'TÂM HỒN CÔ ĐƠN (REMIX)', 'Anh Bằng' , 'Đời không như ước mơ lòng người, cuộc đời xô đẩy mình đến rã rời...', false);
        //$scope.load(); 
        //$scope.insert('Tran', 'Uyen');
        //$scope.select('59963');
        
        $scope.search = null;
        //$scope.$aplly();
        console.log('Total Records in c s:' + $scope.listDanhSach.length);
    };

    $scope.listInsert = function(){
        $scope.insert('59962', 'TÂM HỒN CÔ ĐƠN (REMIX)', 'Anh Bằng' , 'Đời không như ước mơ lòng người, cuộc đời xô đẩy mình đến rã rời...', false);
        $scope.insert('59705', 'ANH', 'Giáng Son' , 'Anh là tình yêu trong tim em dù thời gian có cách xa...', false);
        $scope.insert('59706', 'ANH ĐA TÌNH QUÁ', 'Hồ Hoài Anh' , 'Từng ngày qua đi trong em đã nguôi ngoai, kỷ niệm bên anh...', false);
        $scope.insert('59707', 'ANH LÀ CỦA EM', 'Karik' , 'Chẳng dám hứa quá nhiều chuyện ngày mai...', false);
        $scope.insert('59708', 'ANH MUỐN NÓI', 'Vũ Ngọc Bích' , 'Còn lại điều gì em hỡi khi đôi ta đã mất nhau rồi...', false);
        $scope.insert('59709', 'ANH NHỚ EM NHIỀU LẮM', 'Lý Tuấn Kiệt' , 'Ánh nắng ban mai đang sáng soi cuộc đời anh...', false);
        $scope.insert('59710', 'ANH SẼ ĐỂ EM RA ĐI', 'Vĩ MJ' , 'Từng ngày đi qua với anh nhẹ nhàng vì anh biết...', false);
        $scope.insert('59711', 'ANH TỰ LÀM ANH ĐAU', 'Khánh Đơn' , 'Từ lúc đó khi mà em lạnh lùng gạt tay anh đi nắm tay ai...', false);
        $scope.insert('59712', 'ÂM NHẠC TRONG TÔI', 'Addy Trần' , 'Ngày từng ngày trôi âm thanh xung quanh trôi qua nhanh...', false);
        $scope.insert('59713', 'BA ĐÌNH NẮNG', 'Nhạc: Bùi Công Kỳ Lời: Vũ Hoàng Địch' , 'Gió vút lên ngọn cờ trên kỳ đài phơi phới...', false);
        $scope.insert('59714', 'BÀI HÁT CHO EM', 'Đinh Mạnh Ninh' , 'Anh viết cho em mùa hè yêu thương và anh hát cho em...', false);
        $scope.insert('59715', 'BÀI THƠ KHÔNG ĐOẠN KẾT', 'Lam Phương' , 'Xin gửi cho anh đôi môi hồng đào ngày xưa, một nụ hôn tha thiết...', false);
    };

          /*$scope.insert = function(firstname, lastname) {
              var query = "INSERT INTO people (firstname, lastname) VALUES (?,?)";
              $cordovaSQLite.execute(db, query, [firstname, lastname]).then(function(res) {
                  console.log("INSERT ID -> " + res.insertId);
              }, function (err) {
                  console.error(err);
              });
          }*/

    $scope.insert = function(code, name, noted, lyrics, liked) {
        var query = "INSERT INTO song (code, name, noted, lyrics, liked) VALUES (?,?,?,?,?)";
        $cordovaSQLite.execute(db, query, [code, name, noted, lyrics, liked]).then(function(res) {
            console.log("INSERT ID -> " + res.insertId);
        }, function (err) {
            console.error(err);
        });
    };

    $scope.delteAll = function() {
        var query = "DELETE FROM song";
        $cordovaSQLite.execute(db, query, null).then(function(res) {
            console.log("Delete successful");
        }, function (err) {
            console.error(err);
        });
    };

    $scope.load1 = function() {
  // Execute SELECT statement to load message from database.
        $cordovaSQLite.execute(db, 'SELECT * FROM song ORDER BY code DESC').then(function(res) {
            console.log("Total Records: " + res.rows.length);
            if (res.rows.length > 0) {
                console.log("User data loaded successful, cheers!");
            }else{
                console.log("No data to show");
            }
        },
    function(error) {
        $scope.statusMessage = "Error on loading: " + error.message;
    })
    };





          
       
    $scope.select = function(lastname) {
        var query = "SELECT code, name FROM song WHERE code = ?";
        $cordovaSQLite.execute(db, query, [lastname]).then(function(res) {
          angular.forEach(res.rows, function(item){
                console.log("SELECTED -> " + item.firstname + " " + item.lastname);
          });
            /*if(res.rows.length > 0) {
                console.log("SELECTED -> " + res.rows.item(0).firstname + " " + res.rows.item(0).lastname);
            } else {
                console.log("No results found");
            }*/
        }, function (err) {
            console.error(err);
        });
    }

    $scope.scrollBottom = function() {
        $ionicScrollDelegate.scrollBottom();

    $scope.getItemHeight = function(item) {
        return item.isLetter ? 40 : 100;
    };

    $scope.getContacts = function() {          

      letterHasMatch = {};
      //Filter contacts by $scope.search.
      //Additionally, filter letters so that they only show if there
      //is one or more matching contact
      return contacts.filter(function(item) {
        var itemDoesMatch = !$scope.search || item.isLetter ||
          item.first_name.toLowerCase().indexOf($scope.search.toLowerCase()) > -1 ||
          item.last_name.toLowerCase().indexOf($scope.search.toLowerCase()) > -1;

        //console.log(item.last_name.toString().charAt(0));
        
        //Mark this person's last name letter as 'has a match'
        if (!item.isLetter && itemDoesMatch) {

          var letter = item.last_name.charAt(0).toUpperCase();
          if ( item.last_name.charCodeAt(0) < 65 ){
            letter = "#";
          }
          letterHasMatch[letter] = true;
        }

        return itemDoesMatch;
      }).filter(function(item) {
        //Finally, re-filter all of the letters and take out ones that don't
        //have a match
        if (item.isLetter && !letterHasMatch[item.letter]) {
          return false;
        }
        
        return true;
      });
    };

     
    };
  })

;
